<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/pusher/auth', 'HomeController@authenticate')->name('pusher.authenticate');

Route::post('/home/translate', 'HomeController@translate')->name('home.translate');
Route::post('/home/upload', 'HomeController@upload')->name('home.upload');
Route::post('/transxend/home/translate', 'HomeController@translate')->name('home.translate');

Route::post('/channel/join/{id}', 'ChannelController@join')->name('channel.join');
Route::get('/channel/invite/{unique_channel_id}', 'ChannelController@invite')->name('channel.invite');

Route::resource('channel', 'ChannelController');
