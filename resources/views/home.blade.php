@extends('layouts.app')

@section('content')
<div class="container py-4">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <p>Channels</p>

            <table class="table">
                <tbody>
                @foreach($channels as $channel)
                    <tr>
                        <td>
                            <a class="join-channel" href="javascript:void(0)" data-route="{{ route('channel.join', $channel->id) }}" data-toggle="modal" data-target="#joinChannelModal">
                                <div>{{ $channel->name }}</div>
                            </a>
                        </td>
                        <td>
                            <div class="d-flex">
                                @if($channel->creator_id == Auth::user()->id)
                                <input id="myInput" type="text" name="" value="{{ route('channel.invite', $channel->unique_channel_id) }}" style="position: absolute; top: -1000px;">
                                <button class="btn btn-primary btn-sm mr-2" onclick="myFunction()">Copy text</button>
                                <form action="{{ route('channel.destroy', $channel->id) }}" method="POST">
                                    @method('delete')
                                    @csrf
                                    <button type="button" class="btn btn-danger btn-sm" onclick="
                                        if(confirm('Delete this channel?')) {
                                            $(this).closest('form').submit();
                                        }
                                    ">Delete</button>
                                </form>
                                @endif
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-8">
            <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Create Channel</button>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create Channel</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('channel.store') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Channel Name</label>
                        <input type="text" name="name" class="form-control" placeholder="Enter name" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Channel Password</label>
                        <input type="password" name="password" class="form-control" placeholder="Password" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="joinChannelModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Join Channel</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Channel Password</label>
                        <input type="password" name="password" class="form-control" placeholder="Password" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('.join-channel').click(function(){
        $('#joinChannelModal').find('form').attr('action', $(this).data('route'));
        $('#joinChannelModal').modal('show');
    });

    function myFunction() {
        /* Get the text field */
        var copyText = document.getElementById("myInput");

        /* Select the text field */
        copyText.select();
        copyText.setSelectionRange(0, 99999); /*For mobile devices*/

        /* Copy the text inside the text field */
        document.execCommand("copy");
    }
</script>
@endsection