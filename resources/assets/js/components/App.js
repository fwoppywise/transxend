import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import MediaHandler from '../MediaHandler';
import Pusher from 'pusher-js';
import Peer from 'simple-peer';

const APP_KEY = '7e729446a468449f93c5';

export default class App extends Component {
    constructor() {
        super();

        this.state = {
            hasMedia: false,
            isSharingScreen: false,
            otherUserId: null
        };

        this.user = window.user;
        this.user.stream = null;
        this.peers = {};
        this.members = {};

        this.mediaHandler = new MediaHandler();

        this.channelid = $('#app').data('channelid');
        this.setupPusher(this.channelid);

        this.callTo = this.callTo.bind(this);
        this.setupPusher = this.setupPusher.bind(this);
        this.startPeer = this.startPeer.bind(this);
        this.sendToAllPeers = this.sendToAllPeers.bind(this);
        this.sendMessage = this.sendMessage.bind(this);
        this.sendFile = this.sendFile.bind(this);
        this.handleKeyDown = this.handleKeyDown.bind(this);
        this.leaveCall = this.leaveCall.bind(this);
        this.joinCall = this.joinCall.bind(this);
        this.background = this.background.bind(this);
        this.toggleCamera = this.toggleCamera.bind(this);
        this.shareScreen = this.shareScreen.bind(this);
        this.closePeerConnection = this.closePeerConnection.bind(this);
        this.inputReadURL = this.inputReadURL.bind(this);
        this.changeBackground = this.changeBackground.bind(this);
    }

    UNSAFE_componentWillMount() {
        this.mediaHandler.getPermissions()
            .then((stream) => {
                this.setState({hasMedia: true});
                this.user.stream = stream;

                try {
                    this.myVideo.srcObject = stream;
                } catch (e) {
                    this.myVideo.src = URL.createObjectURL(stream);
                }

                this.myVideo.play();
            })
    }

    setupPusher(channelid) {
        Pusher.logToConsole = true;
        var authEndpoint = (window.location.href.includes('gratest.sakura.ne.jp')) ? '/transxend/pusher/auth' : '/pusher/auth';
        console.log(authEndpoint);
        this.pusher = new Pusher(APP_KEY, {
            authEndpoint: authEndpoint,
            cluster: 'ap3',
            enableStats: true,
            auth: {
                params: this.user.id,
                headers: {
                    'X-CSRF-Token': window.csrfToken
                }
            }
        });
        
        this.pusher.connection.bind('connected', () => console.log('connected'));
        this.pusher.connection.bind('connecting', () => console.log('connecting'));
        this.pusher.connection.bind('connecting_in', () => console.log('connecting_in'));
        this.pusher.connection.bind('disconnected', () => console.log('disconnected'));
        this.pusher.connection.bind('unavailable', () => console.log('unavailable'));
        this.pusher.connection.bind('error', (err) => console.log({err}, 'error'));
        this.pusher.connection.bind('failed', (err) => console.log({err}, 'failed'));

        this.channel = this.pusher.subscribe('presence-video-channel-'+channelid);

        this.channel.bind(`client-userleft-${this.user.id}`, (signal) => {
            console.log(this.peers);

            var userId = signal.userId;

            this.closePeerConnection(userId);

            console.log(this.peers);
        });

        this.channel.bind(`client-background-${this.user.id}`, (signal) => {

            var userId = signal.userId;

            $("#c2-"+userId).toggle();
        });

        this.channel.bind(`client-changebackground-${this.user.id}`, (signal) => {

            var userId = signal.userId;

            $("#c2-"+userId).css('background-image', 'url('+signal.data.src+')');
        });

        this.channel.bind(`client-uploadbackground-${this.user.id}`, (signal) => {
            console.log(signal.data.result);

            var userId = signal.userId;

            $("#c2-"+userId).css('background-image', 'url('+signal.data.result+')');
        });

        this.channel.bind(`client-chatfile-${this.user.id}`, (signal) => {

            var userId = signal.userId;

            if (signal.data.response.ext == 'jpg' || signal.data.response.ext == 'jpeg' || signal.data.response.ext == 'png') {
                var content = '\
                    <a href="'+signal.data.response.path+'" target="_blank">\
                        <img src="'+signal.data.response.path+'" alt="" style="width: 100%;"/>\
                    </a>';
            } else {
                var content = '<a href="'+signal.data.response.path+'" target="_blank">'+signal.data.response.name+'</a>';
            }

            if ($('#chat').data('last') == userId && $('#chat').children().length > 0) {

                $('#chat').append('\
                    <li>\
                        <div class="message-data">\
                        </div>\
                        <div class="message my-message">\
                            '+content+'\
                        </div>\
                    </li>\
                ');
            } else {

                $('#chat').append('\
                    <li>\
                        <div class="message-data">\
                            <span class="message-data-name">'+signal.data.userName+'</span>\
                        </div>\
                        <div class="message my-message">\
                            '+content+'\
                        </div>\
                    </li>\
                ');
            }

            $('#chat').data('last', userId);

            $(".chat-history").animate({ scrollTop: $('.chat-history').prop("scrollHeight")}, 0);
        });

        this.channel.bind(`client-chat-${this.user.id}`, (signal) => {

            var userId = signal.userId;

            if ($('#chat').data('last') == userId && $('#chat').children().length > 0) {

                $('#chat').append('\
                    <li>\
                        <div class="message-data">\
                        </div>\
                        <div class="message my-message">\
                            '+signal.data.message+'\
                        </div>\
                    </li>\
                ');
            } else {

                $('#chat').append('\
                    <li>\
                        <div class="message-data">\
                            <span class="message-data-name">'+signal.data.userName+'</span>\
                        </div>\
                        <div class="message my-message">\
                            '+signal.data.message+'\
                        </div>\
                    </li>\
                ');
            }

            $('#chat').data('last', userId);

            $(".chat-history").animate({ scrollTop: $('.chat-history').prop("scrollHeight")}, 0);
        });

        this.channel.bind(`client-togglecamera-${this.user.id}`, (signal) => {

            var userId = signal.userId;

            $(".video-container #video-"+userId).css('visibility', signal.data.video);
            $(".video-container #c2-"+userId).css('display', signal.data.c2);
            $(".video-container #camera-off-"+userId).css('display', signal.data.camera);
        });

        this.channel.bind(`client-message-${this.user.id}`, (signal) => {

            var final_span1 = $('.speech-results[data-userid='+signal.userId+']').find('.translate-input > .final')[0];
            var interim_span1 = $('.speech-results[data-userid='+signal.userId+']').find('.translate-input > .interim')[0];
            var t_final_span1 = $('.speech-results[data-userid='+signal.userId+']').find('.translate-output > .final')[0];
            var t_interim_span1 = $('.speech-results[data-userid='+signal.userId+']').find('.translate-output > .interim')[0];

            final_span1.innerHTML = '';
            interim_span1.innerHTML = '';
            t_final_span1.innerHTML = '';
            t_interim_span1.innerHTML = '';

            console.log('client-message signaling ' + signal.userId);

            if(signal.data.isFinal)
            {
                final_span1.innerHTML = linebreak(signal.data.final_transcript);
                t_final_span1.innerHTML = linebreak(signal.data.t_final_transcript);
            }
            else
            {
                interim_span1.innerHTML = linebreak(signal.data.interim_transcript);
                t_interim_span1.innerHTML = linebreak(signal.data.t_final_transcript);
            }
        });

        this.channel.bind(`client-startpeer-${this.user.id}`, (signal) => {
            console.log('startpeer ' + signal.userId);
            let peer = this.peers[signal.userId];

            // if peer is not already exists, we got an incoming call
            if(peer === undefined) {
                this.setState({otherUserId: signal.userId});
                peer = this.startPeer(signal.userId, signal.userName, true, this.user);
                this.peers[signal.userId] = peer;
            }
            console.log(this.peers);
        });

        this.channel.bind(`client-signal-${this.user.id}`, (signal) => {
            let peer = this.peers[signal.userId];

            // if peer is not already exists, we got an incoming call
            if(peer === undefined) {
                this.setState({otherUserId: signal.userId});
                peer = this.startPeer(signal.userId, signal.userName, false);
                this.peers[signal.userId] = peer;
            }

            peer.signal(signal.data);
        });

        this.channel.bind(`pusher:subscription_succeeded`, (members) => {
            // $('#members').empty();
            members.each((member) => {
                this.addMember(member);
            });

            this.joinCall();
        });

        this.channel.bind(`pusher:member_added`, (member) => {
            this.addMember(member);
        });

        this.channel.bind(`pusher:member_removed`, (member) => {
            this.removeMember(member);
        });
    }

    addMember(member) {
        $('#member_' + member.id).show();
        this.members[member.id] = member;
        console.log('member ' + member.info.name + ' was added');
    }

    closePeerConnection(userId) {
        // destroy peer
        let peer = this.peers[userId];
        if(peer !== undefined) {
            peer.destroy();
        }

        this.peers[userId] = undefined;

        // reset call button
        $('#member_' + userId).find('a').html('Call');

        // stop video streaming of removed member
        $('.speech-results[data-userid='+userId+']').closest('.video-container').closest('div.col').remove();
    }

    removeMember(member) {
        $('#member_' + member.id).hide();

        this.closePeerConnection(member.id);
        
        this.members[member.id] = undefined;

        console.log('member ' + member.info.name + ' was removed');
    }

    // render transcript for all receivers in call
    sendToAllPeers(event, channel, peers, userId, data) {
        for (var item in peers) {
            console.log('sending to peer ' + item);
            channel.trigger(event + item, {
                type: 'signal',
                userId: userId,
                data: data
            });
        }
    }

    startPeer(userId, userName, initiator = true, thisUser = this.user) {
        console.log('this.user ' + JSON.stringify(thisUser));
        const peer = new Peer({
            initiator,
            stream: thisUser.stream,
            trickle: false
        });

        var mThis = this;

        peer.on('signal', (data) => {
            this.channel.trigger(`client-signal-${userId}`, {
                type: 'signal',
                userId: thisUser.id,
                userName: thisUser.name,
                data: data
            });

            console.log('received signal from ' +  userId + ' with data ' + JSON.stringify(data));

            var channel = this.channel;
            var user = thisUser;
            // var mThis = this;

            if (annyang) {
            
                annyang.start();

                var recognition = new annyang.getSpeechRecognizer();

                $('#select_source_language').on('change', function(){

                    annyang.abort();

                    annyang.setLanguage($(this).val());

                    annyang.resume();
                });

                recognition.interimResults = true;
                recognition.continuous = true;
                var initSoundEnd = false;
                recognition.onresult = function(event) {

                    // disable the drop down so that user cannot select another option while translation is under way
                    $('#select_translate_api').prop('disabled', true);
                    $('#select_translate_language').prop('disabled', true);
                    $('#select_source_language').prop('disabled', true);
                    $('select').prop('disabled', true);

                    // trigger onsoundend event on start
                    if(!initSoundEnd)
                    {
                        recognition.onsoundend(event);
                        initSoundEnd = true;
                    }

                    var interim_transcript = '';
                    var t_interim_transcript = '';
                    final_transcript = '';
                    t_final_transcript = '';
                    final_span.innerHTML = '';
                    interim_span.innerHTML = '';
                    t_final_span.innerHTML = '';
                    t_interim_span.innerHTML = '';
                    // final_span1.innerHTML = '';
                    // interim_span1.innerHTML = '';
                    // t_final_span1.innerHTML = '';
                    // t_interim_span1.innerHTML = '';
                    // final_span2.innerHTML = '';
                    // interim_span2.innerHTML = '';
                    // t_final_span2.innerHTML = '';
                    // t_interim_span2.innerHTML = '';
                    var isFinal = false;
                    for (var i = event.resultIndex; i < event.results.length; ++i) {
                        if (event.results[i].isFinal) {
                            final_transcript += event.results[i][0].transcript;
                            isFinal = true;
                        } else {
                            interim_transcript += event.results[i][0].transcript;
                            isFinal = false;
                        }
                    }

                    var targetLanguage = $('#select_translate_language').val();
                    var sourceLanguage = $('#select_source_language').val();
                    var isRealTime = $('#select_realtime').val();

                    if(isFinal)
                    {

                        console.log('translating transcript: ' + final_transcript);
                        console.log('translating transcript URL-encoded: ' + encodeURIComponent(final_transcript));

                        if($('#select_translate_api').val() == 0)
                        {

                            // YANDEX TRANSLATE API
                            $.post('https://translate.yandex.net/api/v1.5/tr/translate?key=trnsl.1.1.20200420T090458Z.c5240b08369b8a9c.3adffd8811fa6325b6f12bafb49822d48e397a53&text='+encodeURIComponent(final_transcript)+'&lang='+sourceLanguage+'-'+targetLanguage+'&format=plain', function(response){
                                console.log('translated transcript: ' + $(response).find('text').text());

                                t_final_transcript = $(response).find('text').text();

                                final_span.innerHTML = linebreak(final_transcript);
                                t_final_span.innerHTML = linebreak(t_final_transcript);

                                mThis.sendToAllPeers('client-message-', channel, mThis.peers, user.id, {
                                    final_transcript: final_transcript,
                                    interim_transcript: interim_transcript,
                                    t_final_transcript: t_final_transcript,
                                    isFinal: isFinal
                                });

                                $('#select_translate_api').prop('disabled', false);
                                $('#select_translate_language').prop('disabled', false);
                                $('#select_source_language').prop('disabled', false);
                                $('select').prop('disabled', false);
                            });
                        }
                        else
                        {

                            // GOOGLE TRANSLATE API (LIVE ONLY)
                            $.post('/transxend/home/translate', { '_token': window.csrfToken, 'final_transcript': final_transcript, 'target_language': targetLanguage, 'source_language': sourceLanguage }, function(response){
                                console.log('translated transcript: ' + response.final_transcript);

                                t_final_transcript = response.final_transcript;

                                final_span.innerHTML = linebreak(final_transcript);
                                t_final_span.innerHTML = linebreak(t_final_transcript);

                                mThis.sendToAllPeers('client-message-', channel, mThis.peers, user.id, {
                                    final_transcript: final_transcript,
                                    interim_transcript: interim_transcript,
                                    t_final_transcript: t_final_transcript,
                                    isFinal: isFinal
                                });

                                $('#select_translate_api').prop('disabled', false);
                                $('#select_translate_language').prop('disabled', false);
                                $('#select_source_language').prop('disabled', false);
                                $('select').prop('disabled', false);
                            }); 
                        }
                    }
                    else
                    {
                        if(isRealTime == 1)
                        {

                            console.log('translating transcript: ' + interim_transcript);
                            console.log('translating transcript URL-encoded: ' + encodeURIComponent(interim_transcript));

                            if($('#select_translate_api').val() == 0)
                            {

                                // YANDEX TRANSLATE API
                                $.post('https://translate.yandex.net/api/v1.5/tr/translate?key=trnsl.1.1.20200420T090458Z.c5240b08369b8a9c.3adffd8811fa6325b6f12bafb49822d48e397a53&text='+encodeURIComponent(interim_transcript)+'&lang='+sourceLanguage+'-'+targetLanguage+'&format=plain', function(response){
                                    console.log('translated transcript: ' + $(response).find('text').text());

                                    t_interim_transcript = $(response).find('text').text();

                                    interim_span.innerHTML = linebreak(interim_transcript);
                                    t_interim_span.innerHTML = linebreak(t_interim_transcript);

                                    mThis.sendToAllPeers('client-message-', channel, mThis.peers, user.id, {
                                        final_transcript: final_transcript,
                                        interim_transcript: interim_transcript,
                                        t_final_transcript: t_final_transcript,
                                        isFinal: isFinal
                                    });

                                    $('#select_translate_api').prop('disabled', false);
                                    $('#select_translate_language').prop('disabled', false);
                                    $('select').prop('disabled', false);
                                });
                            }
                            else
                            {

                                // GOOGLE TRANSLATE API (LIVE ONLY)
                                $.post('/transxend/home/translate', { '_token': window.csrfToken, 'final_transcript': interim_transcript, 'target_language': targetLanguage, 'source_language': sourceLanguage }, function(response){
                                    console.log('translated transcript: ' + response.final_transcript);

                                    t_final_transcript = response.final_transcript;

                                    interim_span.innerHTML = linebreak(interim_transcript);
                                    t_interim_span.innerHTML = linebreak(t_final_transcript);

                                    mThis.sendToAllPeers('client-message-', channel, mThis.peers, user.id, {
                                        final_transcript: final_transcript,
                                        interim_transcript: interim_transcript,
                                        t_final_transcript: t_final_transcript,
                                        isFinal: isFinal
                                    });

                                    $('#select_translate_api').prop('disabled', false);
                                    $('#select_translate_language').prop('disabled', false);
                                    $('select').prop('disabled', false);
                                }); 
                            }
                        }
                        else
                        {
                            interim_span.innerHTML = linebreak(interim_transcript);

                            t_final_span.innerHTML = '...';

                            mThis.sendToAllPeers('client-message-', channel, mThis.peers, user.id, {
                                final_transcript: final_transcript,
                                interim_transcript: interim_transcript,
                                t_final_transcript: t_final_transcript,
                                isFinal: isFinal
                            });
                        }
                    }
                };

                recognition.onsoundend = function(event) {
                    console.log('onsoundend');
                };
            }

            console.log('calling ' +  userId);
            $('#member_' + userId).find('a').html('<span class="badge badge-success">In Call</span>');
        });

        peer.on('stream', (stream) => {

            if ($('.videos').find('.speech-results[data-userid='+userId+']').length) {
            } else {

                $('.videos').append('\
                    <div class="col p-0">\
                        <div class="video-container">\
                            <h3 id="camera-off-'+userId+'" class="camera-off" style="display: none;">camera off</h3>\
                            <p></p>\
                            <video id="video-'+userId+'" class="user-video"></video>\
                            <div class="speech-results" data-userid="'+userId+'">\
                                <div class="translate-input">\
                                    <span id="final_span1" class="final"></span>\
                                    <span id="interim_span1" class="interim"></span>\
                                </div>\
                                <div class="translate-output">\
                                    <span id="t_final_span1" class="final"></span>\
                                    <span id="t_interim_span1" class="interim"></span>\
                                </div>\
                            </div>\
                            <canvas class="c1" id="c1-'+userId+'" width="422" height="317"></canvas>\
                            <canvas class="c2" id="c2-'+userId+'" width="422" height="317"></canvas>\
                        </div>\
                    </div>\
                ');
            }

            var videoElement = $('.videos').find('.speech-results[data-userid='+userId+']').closest('.video-container').find('video')[0];

            try {
                videoElement.srcObject = stream;
            } catch (e) {
                videoElement.src = URL.createObjectURL(stream);
            }

            $(videoElement).closest('.video-container').find('p').text(userName);

            videoElement.play();

            processor2.doLoad("video-"+userId, "c1-"+userId, "c2-"+userId);

            console.log('stream played for ' + userId);
        });

        peer.on('close', () => {
            let peer = this.peers[userId];
            if(peer !== undefined) {
                peer.destroy();
            }

            this.peers[userId] = undefined;
        });

        return peer;
    }

    callTo(userId, userName) {
        if(!this.peers[userId])
        {
            $('#member_' + userId).find('a').text('Calling');
            this.peers[userId] = this.startPeer(userId, userName, true);

            if (Object.keys(this.peers).length >= 2) {
                for (var peer in this.peers) {
                    if (peer != userId) {
                        console.log('SENDING TO ' + peer);
                        this.channel.trigger(`client-startpeer-${peer}`, {
                            type: 'signal',
                            userId: userId,
                            userName: userName
                        });
                    }
                }
            }
        }
        else
        {
            alert('User is already calling');
        }
    }

    shareScreen(e) {
        if (this.state.isSharingScreen) {
            this.user.stream.getTracks()
                .forEach(track => track.stop());

            this.mediaHandler.getPermissions()
                .then((stream) => {
                    this.setState({isSharingScreen: false});
                    this.user.stream = stream;

                    for (var member in this.members) {
                        var member = this.members[member];

                        if(member && member.id != this.user.id)
                        {
                            this.closePeerConnection(member.id);

                            this.peers[member.id] = this.startPeer(member.id, member.info.name, true);

                            // change button text
                            $("#shareScreenBtn").text('Start Share Screen');
                            $("#shareScreenBtn").removeClass('btn-warning');
                            $("#shareScreenBtn").addClass('btn-primary');
                        }
                    }
                })
        } else {
            this.mediaHandler.getShareScreenPermissions()
                .then((stream) => {
                    this.setState({isSharingScreen: true});
                    this.user.stream = stream;

                    for (var member in this.members) {
                        var member = this.members[member];

                        if(member && member.id != this.user.id)
                        {
                            this.closePeerConnection(member.id);

                            this.peers[member.id] = this.startPeer(member.id, member.info.name, true);

                            // change button text
                            $("#shareScreenBtn").text('Stop Share Screen');
                            $("#shareScreenBtn").removeClass('btn-primary');
                            $("#shareScreenBtn").addClass('btn-warning');
                        }
                    }
                })
        }
    }

    joinCall() {
        console.log('joining call with other members');
        console.log(this.members);
        console.log(Object.keys(this.members).length);

        for (var member in this.members) {
            var member = this.members[member];
            console.log(member);
            console.log(member.id);
            console.log(this.user.id);
            console.log(this.peers);
            console.log(member.id != this.user.id);

            if(member && !this.peers[member.id] && member.id != this.user.id)
            {
                console.log('CALLING ' + member.info.name);
                this.peers[member.id] = this.startPeer(member.id, member.info.name, true);

                // for (var k = 1; k <= Object.keys(this.members).length; k++) {
                //     var kmember = this.members[k];

                //     if(kmember && kmember.id != member.id)
                //     {
                //         console.log('TELLING ' + member.info.name + ' TO CALL ' + kmember.info.name);
                //         this.channel.trigger(`client-startpeer-${member.id}`, {
                //             type: 'signal',
                //             userId: kmember.id,
                //             userName: kmember.info.name
                //         });
                //     }
                // }
            }
        }
    }

    sendMessage() {
        if ($('#message').val().length > 0) {
            this.sendToAllPeers('client-chat-', this.channel, this.peers, this.user.id, {
                userName: this.user.name,
                message: $('#message').val()
            });

            if ($('#chat').data('last') == this.user.id && $('#chat').children().length > 0) {

                $('#chat').append('\
                    <li class="clearfix">\
                        <div class="message-data align-right">\
                        </div>\
                        <div class="message other-message float-right">\
                            '+$('#message').val()+'\
                        </div>\
                    </li>\
                ');
            } else {

                $('#chat').append('\
                    <li class="clearfix">\
                        <div class="message-data align-right">\
                            <span class="message-data-name" >'+this.user.name+'</span>\
                        </div>\
                        <div class="message other-message float-right">\
                            '+$('#message').val()+'\
                        </div>\
                    </li>\
                ');
            }

            $('#chat').data('last', this.user.id);

            $('#message').val('');

            $(".chat-history").animate({ scrollTop: $('.chat-history').prop("scrollHeight")}, 0);
        }
    }

    sendFile(e) {
        var mThis = this;
        console.log(e.target.files);
        if (e.target.files && e.target.files[0]) {
            var form_data = new FormData();
            form_data.append('file', e.target.files[0]);

            $.ajax({
                headers: {
                    'X-CSRF-Token': window.csrfToken
                },
                enctype: 'multipart/form-data',
                url: '/transxend/home/upload',
                type: 'POST',
                data: form_data,
                success: function (response) {
                    console.log(response);
                    mThis.sendToAllPeers('client-chatfile-', mThis.channel, mThis.peers, mThis.user.id, {
                        userName: mThis.user.name,
                        response: response
                    });

                    if (response.ext == 'jpg' || response.ext == 'jpeg' || response.ext == 'png') {
                        var content = '\
                            <a href="'+response.path+'" target="_blank">\
                                <img src="'+response.path+'" alt="" style="width: 100%;"/>\
                            </a>';
                    } else {
                        var content = '<a href="'+response.path+'" target="_blank">'+response.name+'</a>';
                    }

                    if ($('#chat').data('last') == mThis.user.id && $('#chat').children().length > 0) {

                        $('#chat').append('\
                            <li class="clearfix">\
                                <div class="message-data align-right">\
                                </div>\
                                <div class="message other-message float-right">\
                                    '+content+'\
                                </div>\
                            </li>\
                        ');
                    } else {

                        $('#chat').append('\
                            <li class="clearfix">\
                                <div class="message-data align-right">\
                                    <span class="message-data-name" >'+mThis.user.name+'</span>\
                                </div>\
                                <div class="message other-message float-right">\
                                    '+content+'\
                                </div>\
                            </li>\
                        ');
                    }

                    $('#chat').data('last', mThis.user.id);

                    $('#message').val('');

                    $(".chat-history").animate({ scrollTop: $('.chat-history').prop("scrollHeight")}, 0);
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    }

    handleKeyDown(event) {
        if(event.charCode === 13) {
            this.sendMessage();
        }
    }

    leaveCall() {

        if (confirm('Leave this channel?')) {
            // send to all peers that this user left
            this.sendToAllPeers('client-userleft-', this.channel, this.peers, this.user.id, {});

            // remove video stream for all peers
            for (var item in this.peers) {
               this.closePeerConnection(item);
            }

            window.location.replace('/transxend/home');
        }
    }

    background() {
        this.sendToAllPeers('client-background-', this.channel, this.peers, this.user.id, {});

        $('#c2').toggle();

        if ($('#c2').css('display') == 'none') {
            $('#toggleBackgroundBtn').text("Show Background");
        } else {
            $('#toggleBackgroundBtn').text("Hide Background");
        }
    }

    toggleCamera() {
        if ($('.video-container #my-video').css('visibility') == 'hidden') {
            $('.video-container #my-video').css('visibility', 'visible');

            if ($('.video-container #c2').css('display') == 'none') {
                $('.video-container #c2').css('display', 'block');
            }

            $('#toggleCameraBtn').text("Hide Camera");
        } else {
            $('.video-container #my-video').css('visibility', 'hidden');

            if ($('.video-container #c2').css('display') == 'block') {
                $('.video-container #c2').css('display', 'none');
            }

            $('#toggleCameraBtn').text("Show Camera");
        }
        $('.video-container #camera-off').toggle();

        this.sendToAllPeers('client-togglecamera-', this.channel, this.peers, this.user.id, {
            video: $('.video-container #my-video').css('visibility'),
            c2: $('.video-container #c2').css('display'),
            camera: $('.video-container #camera-off').css('display')
        });
    }

    inputReadURL(e) {
        var mThis = this;
        if (e.target.files && e.target.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                mThis.sendToAllPeers('client-uploadbackground-', mThis.channel, mThis.peers, mThis.user.id, {
                    'result': e.target.result
                });

                $('#c2').css('background-image', 'url('+e.target.result+')');
            };

            reader.readAsDataURL(e.target.files[0]);
        }
    }


    changeBackground(e) {
        this.sendToAllPeers('client-changebackground-', this.channel, this.peers, this.user.id, {
            'src': $(e.target).attr('src')
        });

        $('canvas.my-c2').css('background-image', 'url('+$(e.target).attr('src')+')');
        $('#exampleModal').modal('hide');
    }

    render() {
        return (
            <div className="App">
                <div className="row">
                    <div className="col-12 col-md-6 col-lg-8">
                        <div className="videos row">
                            {/*<div className="col p-0">
                                <div className="video-container">
                                    <p></p>
                                    <video className="user-video" ref={(ref) => {this.userVideo = ref;}}></video>
                                    <div className="speech-results">
                                        <div className="translate-input">
                                            <span id="final_span1" className="final"></span>
                                            <span id="interim_span1" className="interim"></span>
                                        </div>
                                        <div className="translate-output">
                                            <span id="t_final_span1" className="final"></span>
                                            <span id="t_interim_span1" className="interim"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="secondVideo" className="col p-0">
                                <div className="video-container">
                                    <p></p>
                                    <video className="user-video" ref={(ref) => {this.userVideo1 = ref;}}></video>
                                    <div className="speech-results">
                                        <div className="translate-input">
                                            <span id="final_span2" className="final"></span>
                                            <span id="interim_span2" className="interim"></span>
                                        </div>
                                        <div className="translate-output">
                                            <span id="t_final_span2" className="final"></span>
                                            <span id="t_interim_span2" className="interim"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>*/}
                        </div>
                        <div className="row">
                            <div className="col-12 col-lg-4">
                                <p>
                                    <button id="shareScreenBtn" type="button" className="btn btn-sm btn-primary" onClick={this.shareScreen}>Start Share Screen</button>
                                </p>
                                <p>
                                    <button id="toggleCameraBtn" type="button" className="btn btn-sm btn-primary" onClick={this.toggleCamera}>Hide Camera</button>
                                </p>
                                <p>
                                    <button id="toggleBackgroundBtn" type="button" className="btn btn-sm btn-primary" onClick={this.background}>Show Background</button>
                                </p>
                                <p>
                                <button type="button" className="btn btn-sm btn-primary" data-toggle="modal" data-target="#exampleModal">Choose Background Image</button>
                                </p>
                                <p>
                                    <input id="uploadBtnInput" type="file" accept="image/*" onChange={this.inputReadURL}/>
                                <button id="uploadBtn" type="button" className="btn btn-sm btn-primary">Upload Background Image</button>
                                </p>
                                <p>
                                    <button id="leaveCallBtn" type="button" className="btn btn-sm btn-danger" onClick={this.leaveCall}>Leave Channel</button>
                                </p>
                            </div>
                            <div className="controls col-12 col-lg-4">
                                <p>
                                    <label htmlFor="select_translate_api">Translation API</label>
                                    <select id="select_translate_api" defaultValue="1">
                                      <option value="0">Yandex Translate API</option>
                                      <option value="1">Google Translate API</option>
                                    </select>
                                    </p>
                                    <p>
                                    <label htmlFor="select_realtime">Real-time translation</label>
                                    <select id="select_realtime" defaultValue="0">
                                      <option value="0">Off</option>
                                      <option value="1">On</option>
                                    </select>
                                    </p>
                                    <p>
                                    <label htmlFor="select_source_language">Input Language</label>
                                    <select id="select_source_language" defaultValue="en">
                                          <option value="en">English</option>
                                          <option value="vi">Vietnamese</option>
                                          <option value="ja">Japanese</option>
                                    </select>
                                    </p>
                                    <p>
                                    <label htmlFor="select_translate_language">Output Language</label>
                                    <select id="select_translate_language" defaultValue="ja">
                                          <option value="en">English</option>
                                          <option value="vi">Vietnamese</option>
                                          <option value="ja">Japanese</option>
                                    </select>
                                </p>
                            </div>
                            <div className="col-12 col-lg-4 p-0">
                                <div className="video-container">
                                    <h3 id="camera-off" className="camera-off" style={{display: "none"}}>camera off</h3>
                                    <video id="my-video" autoPlay="{false}" className="my-video" ref={(ref) => {this.myVideo = ref;}}></video>
                                    <div className="speech-results">
                                        <div className="translate-input">
                                            <span id="final_span" className="final"></span>
                                            <span id="interim_span" className="interim"></span>
                                        </div>
                                        <div className="translate-output">
                                            <span id="t_final_span" className="final"></span>
                                            <span id="t_interim_span" className="interim"></span>
                                        </div>
                                    </div>
                                    <canvas className="c1 my-c1" id="c1" width="422" height="317"></canvas>
                                    <canvas className="c2 my-c2" id="c2" width="422" height="317"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="sidebar col-12 col-md-6 col-lg-4">
                        <div className="chat">
                            <div className="chat-history">
                                <ul id="chat">
                                </ul>
                            </div>
                            <div className="chat-message clearfix">
                                <input name="message-to-send" id="message" placeholder="Type your message" onKeyPress={this.handleKeyDown}></input>
                                <input type="hidden" type="file" id="sendFileInput" onChange={this.sendFile}/>
                                <button id="sendFile">File</button>
                                <button onClick={() => this.sendMessage()}>Send</button>
                            </div>
                        </div>
                    </div>
                </div>
            
                <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div className="modal-dialog" role="document">
                    <div className="modal-content">
                      <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalLabel">Choose Background Image</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div className="modal-body">
                        <div className="row">
                            <div className="col-6 mb-2">
                                <a href="javscript:void(0)" onClick={this.changeBackground}>
                                    <div>
                                        <img src="/transxend/public/images/img1.jpg" alt=""></img>
                                    </div>
                                </a>
                            </div>
                            <div className="col-6 mb-2">
                                <a href="javscript:void(0)" onClick={this.changeBackground}>
                                    <div>
                                        <img src="/transxend/public/images/img2.jpg" alt=""></img>
                                    </div>
                                </a>
                            </div>
                            <div className="col-6">
                                <a href="javscript:void(0)" onClick={this.changeBackground}>
                                    <div>
                                        <img src="/transxend/public/images/img3.jpg" alt=""></img>
                                    </div>
                                </a>
                            </div>
                            <div className="col-6">
                                <a href="javscript:void(0)" onClick={this.changeBackground}>
                                    <div>
                                        <img src="/transxend/public/images/img4.jpg" alt=""></img>
                                    </div>
                                </a>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        );
    }
}

if (document.getElementById('app')) {
    var translator = ReactDOM.render(<App />, document.getElementById('app'));
    var final_span = document.getElementById('final_span');
    var interim_span = document.getElementById('interim_span');
}

$(document).ready(function(){

    $('input[type=file]').hide();

    $('#uploadBtn').click(function(){
        $('#uploadBtnInput').click();
    });

    $('#sendFile').click(function(){
        $('#sendFileInput').click();
    });
});

var final_transcript = '';
var interim_transcript = '';
var t_final_transcript = '';
var t_interim_transcript = '';

var first_char = /\S/;
function capitalize(s) {
  return s.replace(first_char, function(m) { return m.toUpperCase(); });
}

var two_line = /\n\n/g;
var one_line = /\n/g;
function linebreak(s) {
  return s.replace(two_line, '<p></p>').replace(one_line, '<br>');
}

let l_r = 131,
    l_g = 255,
    l_b = 137,

    d_r = 0,
    d_g = 148,
    d_b = 0;

let tolerance = 0.05;
let height;
let width;

let processor = {
    timerCallback: function() {
      if (this.video.paused || this.video.ended) {
        return;
      }
      this.computeFrame();
      let self = this;
      setTimeout(function () {
          self.timerCallback();
        }, 0);
    },
  
    doLoad: function(video, c1, c2) {
      this.video = document.getElementById(video);
      this.c1 = document.getElementById(c1);
      this.ctx1 = this.c1.getContext("2d");
      this.c2 = document.getElementById(c2);
      this.ctx2 = this.c2.getContext("2d");
      this.ctx1.imageSmoothingEnabled = true;
      this.ctx2.imageSmoothingEnabled = true;
      let self = this;
      this.video.addEventListener("play", function() {
        }, false);
      this.video.addEventListener("loadedmetadata", function () {
        console.log('loadedmetadata');
            // retrieve dimensions
            height = this.videoHeight;
            width = this.videoWidth;
            console.log(width);
            console.log(height);
          self.width = width;
          self.height = height;
          self.c1.width = width;
          self.c1.height = height;
          self.c2.width = width;
          self.c2.height = height;
          self.timerCallback();
        }, false );
    },

    calculateDistance: function(c, min, max) {
      if(c < min) return min - c;
      if(c > max) return c - max;

      return 0;
    },
  
    computeFrame: function() {
      this.c1.width = width;
      this.c1.height = height;
      this.c2.width = width;
      this.c2.height = height;
      this.ctx1.drawImage(this.video, 0, 0, width, height);
      let frame = this.ctx1.getImageData(0, 0, width, height);
          let l = frame.data.length / 4;
  
      for (let i = 0; i < l; i++) {
        let r = frame.data[i * 4 + 0];
        let g = frame.data[i * 4 + 1];
        let b = frame.data[i * 4 + 2];
        let difference = this.calculateDistance(r, d_r, l_r) + 
                       this.calculateDistance(g, d_g, l_g) +
                       this.calculateDistance(b, d_b, l_b);
        difference /= (255 * 3); // convert to percent
        if (difference < tolerance)
        {
            frame.data[i * 4 + 3] = 0;
        }
      }
      this.ctx1.imageSmoothingEnabled = true;
      this.ctx2.imageSmoothingEnabled = true;
      this.ctx2.putImageData(frame, 0, 0);
      return;
    }
  };

let processor2 = {
    timerCallback: function() {
      if (this.video.paused || this.video.ended) {
        return;
      }
      this.computeFrame();
      let self = this;
      setTimeout(function () {
          self.timerCallback();
        }, 0);
    },
  
    doLoad: function(video, c1, c2) {
      this.video = document.getElementById(video);
      this.c1 = document.getElementById(c1);
      this.ctx1 = this.c1.getContext("2d");
      this.c2 = document.getElementById(c2);
      this.ctx2 = this.c2.getContext("2d");
      this.ctx1.imageSmoothingEnabled = true;
      this.ctx2.imageSmoothingEnabled = true;
      let self = this;
      this.video.addEventListener("play", function() {
        }, false);
      this.video.addEventListener("loadedmetadata", function () {
        console.log('loadedmetadata');
            // retrieve dimensions
            height = this.videoHeight;
            width = this.videoWidth;
            console.log(width);
            console.log(height);
          self.width = width;
          self.height = height;
          self.c1.width = width;
          self.c1.height = height;
          self.c2.width = width;
          self.c2.height = height;
          self.c1.style.maxWidth = "800px";
          self.c2.style.maxWidth = "800px";
          self.timerCallback();
        }, false );
    },

    calculateDistance: function(c, min, max) {
      if(c < min) return min - c;
      if(c > max) return c - max;

      return 0;
    },
  
    computeFrame: function() {
      this.c1.width = width;
      this.c1.height = height;
      this.c2.width = width;
      this.c2.height = height;
      this.ctx1.drawImage(this.video, 0, 0, width, height);
      let frame = this.ctx1.getImageData(0, 0, width, height);
          let l = frame.data.length / 4;
  
      for (let i = 0; i < l; i++) {
        let r = frame.data[i * 4 + 0];
        let g = frame.data[i * 4 + 1];
        let b = frame.data[i * 4 + 2];
        let difference = this.calculateDistance(r, d_r, l_r) + 
                       this.calculateDistance(g, d_g, l_g) +
                       this.calculateDistance(b, d_b, l_b);
        difference /= (255 * 3); // convert to percent
        if (difference < tolerance)
        {
            frame.data[i * 4 + 3] = 0;
        }
      }
      this.ctx1.imageSmoothingEnabled = true;
      this.ctx2.imageSmoothingEnabled = true;
      this.ctx2.putImageData(frame, 0, 0);
      return;
    }
  };

document.addEventListener("DOMContentLoaded", () => {
  processor.doLoad("my-video", "c1", "c2");
});