export default class MediaHandler {
    getPermissions() {
        return new Promise((resolve, rej) => {

            if (hasGetUserMedia()) {
              // Good to go!
              // alert('getUserMedia() is supported by your browser');
            } else {
              alert('getUserMedia() is not supported by your browser');
              return false;
            }

            navigator.mediaDevices.getUserMedia({video: true, audio: true})
                .then((stream) => {
                    resolve(stream);
                })
                .catch(err => {
                    throw new Error(`Unable to fetch stream ${err}`);
                })
        });
    }

    getShareScreenPermissions() {
        return new Promise((resolve, rej) => {

            if (hasGetUserMedia()) {
              // Good to go!
              // alert('getUserMedia() is supported by your browser');
            } else {
              alert('getUserMedia() is not supported by your browser');
              return false;
            }

            navigator.mediaDevices.getDisplayMedia({video:true})
                .then((stream) => {
                    resolve(stream);
                })
                .catch(err => {
                    throw new Error(`Unable to fetch stream ${err}`);
                })
        });
    }
}

function hasGetUserMedia() {
  return !!(navigator.mediaDevices &&
    navigator.mediaDevices.getUserMedia);
}