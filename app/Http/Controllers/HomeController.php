<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use \Pusher\Pusher;
use App\User;
use App\Channel;
use Stichoza\GoogleTranslate\GoogleTranslate;
use Auth;
use Route;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $channels = Channel::get();

        return view('home')->with(compact('channels'));
    }

    public function upload(Request $request)
    {
        $file = $request->file('file');
        $path = $request->file('file')->store('public');

        return response()->json(['success' => true, 'path' => "/transxend/" . str_replace('public', 'storage/app/public', $path), 'name' => $file->getClientOriginalName(), 'ext' => $file->getClientOriginalExtension()]);
    }

    public function translate(Request $request)
    {
        $tr = new GoogleTranslate();
        $tr
            ->setSource($request->source_language)
            ->setTarget($request->target_language);

        $final_transcript = $request->final_transcript ? $tr->translate($request->final_transcript) : '';

        return response()->json(['success' => true, 'final_transcript' => $final_transcript]);
    }

    public function authenticate(Request $request)
    {
        $socketId = $request->socket_id;
        $channelName = $request->channel_name;

        $pusher = new Pusher(env('PUSHER_APP_KEY'), env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'), [
            'cluster' => 'ap3',
            'encrypted' => true,
            'useTLS' => true
        ]);

        $presence_data = ['name' => auth()->user()->name];
        $key = $pusher->presence_auth($channelName, $socketId, auth()->id(), $presence_data);

        return response($key);
    }
}
