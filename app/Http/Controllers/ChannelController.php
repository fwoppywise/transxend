<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\User;
use App\Channel;
use Hash;
use Auth;

class ChannelController extends Controller
{
    public function join(Request $request, $id)
    {
        $channel = Channel::find($id);

        if (Hash::check($request->password, $channel->password)) {

            return view('channel.index')->with(compact('channel'));
        }

        return redirect()->back();
    }

    public function invite(Request $request, $unique_channel_id)
    {
        $channel = Channel::where('unique_channel_id', $unique_channel_id)->first();

        if ($channel) {

            return view('channel.index')->with(compact('channel'));
        }

        abort(404);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $channel = Channel::create([
            'name' => $request->name,
            'password' => Hash::make($request->password),
            'unique_channel_id' => Str::random(10),
            'creator_id' => Auth::user()->id,
        ]);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $channel = Channel::find($id);
        $channel->delete();

        return redirect()->back();
    }
}
